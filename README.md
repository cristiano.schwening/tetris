
# Repositório da cultura da engenharia

Seja bem-vinda(o) ao **Tetris**, repositório da cultura da engenharia do LabSoft =), aqui você vai encontrar vários itens importantes e também muito úteis que todo profissional da engenharia precisa conhecer. Entender bem tudo isso é uma ótima maneira de familiarizar-se com o nosso jeito de trabalhar e de ser empresa.

Essa plataforma busca para disseminar o conhecimento da cultura organizacional para desenvolvimento de software!

Para entender cada um dos itens que você tem acesso, verifique as seções abaixo.

### **Nossa cultura** ###
----

- [Sobre Nós](/nos/sobre-nos.md)

### **Como trabalhamos (Processos)** ###
----

- Como estamos estruturados: Conheça como estamos estruturados e para que servem cada um dos agrupamentos.
  - [Desenvolvimento](/process/develop/index.md)
  - [DEVOPS](/process/devops/index.md)
  - [UX](/process/ux/index.md)
  - [GTD](/process/gtd/index.md)


- Como tomamos nossas decisões: Nossas decisões são tomadas de uma maneira muito interessante. Nesse documento você vai perceber que todos podem e devem opinar. Combinamos até um jeito bem legal de fazermos isso.
- Trabalho remoto: Existem muitos benefícios em trabalharmos de maneira remota e, fazer isso da melhor maneira possível é fundamental.
- Trabalho e comunicação assíncrona: Gostamos de ter a melhor comunicação possível. Entender sobre comunicação assíncrona é fundamental, por isso criamos esse documento.
- Qual idioma usamos?: Você deve ter percebido que às vezes escrevemos as coisas em inglês, às vezes em português. Temos um combinado para facilitar as coisas.
- Cultura data-driven: Nós adotamos uma cultura orientada a dados, que permite embasamento para decisões de produtos, preditibilidade e democratização dos dados.

### **Materiais Compartilhados com cliente** ###
----
Essa área tem como propósito armazenar diversos materiais e referências técnicas associados que são utilizado pelo cliente ou que o mesmo forneceu em algum momento.

- [CXXXX](/clientes/XXX/index.md)



### **Gestão de Pessoas** ###
----
- [Nossa gestão de pessoas](/career/index.md)
- [Processo Seletivo](/career/index.md)


### **Nossas publicações (internas e externas)** ###
----
- Apresentação (18/09/18) - Cristiano (https://docs.google.com/presentation/d/1cJg5Ybboue6kFYWUnEq2TUzZC-0m-oD2E1Ad41mbIdU/edit#slide=id.p)

### **Sites de empresas que serviram de inspiração** ###
- VEK
- Grupo Zap (https://grupozap.github.io/cultura/)

