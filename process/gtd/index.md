﻿### **Grupo de Transformação Digital** ###
----

É o grupo responsável pela facilitação na implementação de práticas ágeis nos squads e áreas do LabSoft. 

Possui pessoas que auxiliam os squads a diagnosticar e propõem intervenções que possam ajudar os squads a melhorar como eles identificam e resolvem problemas e tomam decisões, para aumentar a efetividade e coesão.

- **Práticas de Facilitação**

  * [Template: Product Vision Board](/process/product/templates/The_Product_Vision_Board.pdf)
  * [Dinâmica/Retrospectiva: Vagão do Trem](/process/gtd/dinamica-vagao.md)


- **Artefatos e Cerimônias**
  * [Tipos de Product Backlogs](/process/develop/products-backlog.md)  
  
- **Papéis**
  * [Gestor do Processo](/process/gestor-processo.md)

