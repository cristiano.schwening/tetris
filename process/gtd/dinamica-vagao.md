﻿### Dinâmica Vagão do Trem

**Objetivo:** 

Auxiliar squads que estavam em momentos meio que “nebuloso”, ou seja, com mudanças frequentes durante o período de uma ou mais entregas.

**Introdução**

Dado que o squad seja um trem com vagões, de forma colaborativa o squad deve desenhar uma ferrovia que remeta ao período da sprint/interação até chegar ao trem. 

**Regras para utilização dos elementos:** 

* <u>Retas</u>: Período tranquilo sem maiores problemas
* <u>Curvas</u>: Problemas encontrados pelo squad na iteração que ocasionou desvios
* <u>Estações</u>: Acontecimentos bons ou ruins
* <u>Cruzamentos</u>: Interação junto a outros squads, fornecedores ou clientes
* <u>Semáforos</u>: Demonstra como foi a interação com outros squads, clientes ou fornecedores, onde:
    - <u>Verde</u>: foi tranquila sem problemas
    - <u>Amarela</u>: Tivemos algum work around na interação
    - <u>Vermelha</u>: Tivemos impedimentos.
    
    
**Exemplo de aplicação da dinâmica no LabSoft:** 

![Exemplo de aplicação da dinâmica no LabSoft](img/exemplo-vagao.jpg)