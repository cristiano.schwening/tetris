## Produtos em operação ##

<table>
  <tr>
    <th>VP</th>
    <th>Sigla</th>
    <th>Denominação</th>
    <th>Nome no Cliente</th>
    <th></th>
    <th>VP</th>
    <th>Sigla</th>
    <th>Denominação</th>
    <th>Nome no Cliente</th>    
  </tr>
  <tr>
    <td>????</td>
    <td>Sonar FRQ</td>
    <td>Franquias</td>
    <td>????</td>
    <td></td>  
    <td>????</td>
    <td>Sonar EMP</td>
    <td>Empresas</td>
    <td>????</td>
  </tr>
  <tr>
    <td>????</td>
    <td>Sonar CRP</td>
    <td>Corporate</td>
    <td>????</td>
    <td></td> 
    <td>????</td>
    <td>Sonar NMC</td>
    <td>Novos Mercados</td>
    <td>????</td>
  </tr>
  <tr>
    <td>????</td>
    <td>Sonar ARV</td>
    <td>Hunter</td>
    <td>????</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>????</td>
    <td>Varejo GV</td>
    <td>Gerente Virtual</td>
    <td>????</td>
    <td></td>
    <td>????</td>
    <td>Varejo ISO</td>
    <td>????</td>
    <td>????</td>
  </tr>
  <tr>
    <td>????</td>
    <td>Varejo CRD</td>
    <td>ISO Credenciamento</td>
    <td>????</td>
    <td></td>
    <td>????</td>
    <td>Varejo DDN</td>
    <td>Bancos</td>
    <td>????</td>
  </tr>
  <tr>
    <td>????</td>
    <td>Varejo ACP</td>
    <td>Ação Comercial Pontual</td>
    <td>????</td>
    <td></td>
    <td>????</td>
    <td>Varejo VRJ</td>
    <td>Varejo</td>
    <td>????</td>
  </tr>
  <tr>
    <td>????</td>
    <td>Bancos BNC</td>
    <td>Gestão de Taxas e movimentação</td>
    <td>????</td>
    <td></td>
    <td>????</td>
    <td>CRE</td>
    <td>Courrier</td>
    <td>????</td>
  </tr>
  <tr>
    <td>????</td>
    <td>NITRO</td>
    <td>Ofertas On-line</td>
    <td>????</td>
  </tr>
</table>

## Roadmap dos Produtos (só Épicos) ##
(formato de uso: Horizonte>Objetivos>Iniciativas(Épicos Jira)-Produto[tags])


Os principais recursos que planejamos implementar no futuro próximo. 
Para obter uma visão geral mais completa dos recursos planejados e do trabalho atual, consulte os registros no Jira.

### Atual  ###
- Objetivo A
  - Épico 1 - SiglaProduto [xx] - Responsável


### Médio Prazo ###
- Objetivo A
  - Épico 1 - SiglaProduto [xx] - Responsável


### Futuro ###
- Objetivo A
  - Épico 1 - SiglaProduto [xx] - Responsável

