### Lista de práticas adotadas para a utilização racional e comunitária do formulário de tarefas do Jira. ###
----

- Preciso informar visualmente que uma tarefa (épico/história/subtarefas) está com impedimento (dificuldades, revisão de análise,...):
     - utilizar a opção **Adicionar Flag e comentar** que pode ser alcançada ao clicar com o botão direito do mouse sobre uma tarefa que está em um quadro. No comentário relate brevemente o motivo do impedimento, isso permite que os envolvidos tenham conhecimento. Quando um impedimento for resolvido, a pessoa que o criou deverá ir no mesmo menu na opção **Remover a Flag** nesse caso o comentário é opcional. Atenção: essa funcionalidade FLAG do Jira não é acessada diretamente pelo formulário de edição da tarefa.


- Desejo incluir um link para outra tarefa na descrição ou em um comentário da tarefa que estou editando: 
     - na caixa de descrição da tarefa coloque entre colchetes o código da outra tarefa (exemplo: [CVD-0000]). O jira fará o link automaticamente.