﻿### Template do padrão de especificação de incidentes no Jira. ###
----
As questões abaixo devem ser incluídas na descrição de um incidente. Abaixo da questão deve ser descritas as informações requeridas. As seções opcionais não presam ser utilizadas caso no incidente a questão não for abordada.



1)    Descrição da Ocorrência (obrigatório)

2)    Comportamento Esperado (opcional no caso de o Cliente estar questionando alguma regra)

3)    Diretoria (obrigatório)

4)    Usuário (opcional — obrigatório pelo menos um caso para problemas de carteira, oferta, taxas, margem, simulação de taxas, contrato, credenciamento)

5)    EC (opcional — obrigatório pelo menos um caso para problemas de carteira, oferta, taxas, margem, simulação de taxas, contrato, credenciamento)

6)    Versão (obrigatório — especificar versão de cada diretoria)

7)    Ambiente (obrigatório — hml/prod)

8)    Anexos (opcionais)