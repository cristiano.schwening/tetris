## Padrões para utilização de estilos CSS ##

Organização das propriedades de estilo:

- Alfabeticamente ascendente. 
>Ex:
>```
>background: #000000;
>border: #AAAAAA;
>float: left;
>font: verdana;
>position: relative;
>```

- Abreviação de propriedades:

Margin.
>Ex:
>```
>margin: top right bottom left;
>margin: 10px 0 1px 5px;
>```

Padding. 
>Ex:
>```
>Sintaxe -> padding: top right bottom left;
>padding: 10px 0 1px 5px;
>```

Background
>Ex:
>```
>Sintaxe -> background: color img repeat attachment position;
>background: #ffc url(“img/fundo.jpg”) no-repeat fixed 20px 10px;
>```

Font
>Ex:
>```
>Sintaxe -> font: style variant weight size height family;
>font: italic small-caps bold 11px 10px Arial, Helvetica, Sans-serif;
>```

List
>Ex:
>```
>Sintaxe -> list-style: type position image;
>List-style: square inside url(“img/marcador.jpg”);
>```

Outline
>Ex:
>```
>Sintaxe -> outline: color style width;
>outline: #000 solid 1px;
>```

- Comentários na folha de estilos

Comentário de div em primeiro nível
>Ex:
>```
>/* DESCRICAO DO SELETOR */
>```

Comentário de div em segundo nível
>Ex:
>```
>/** DESCRICAO DO SELETOR **/
>```

Comentário de div em terceiro nível
>Ex:
>```
>/*** DESCRICAO DO SELETOR ***/
>```

Exemplo ilustrativo


![alt text](/process/develop/img/exemploGuiaCss.PNG)