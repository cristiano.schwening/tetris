Um incidente é um defeito, erro, falha que faz com que o sistema se comporte incorretamente ou o impeça de atender aos requisitos do produto. O nível de impacto de um incidente pode variar de bloquear toda uma funcionalidade ou um bug de usabilidade de recursos. Um bug deve estar sempre vinculado a um nível de gravidade.

Consulte nossos níveis de gravidade e se o incidente também é uma regressão ou não, o processo de triagem deve começar o mais rápido possível.

Certifique-se de que o Gerente de Produto da área relacionada esteja envolvido para priorizar o problema, caso necessário.


Regressões
----
Uma regressão implica que uma **funcionalidade verificada anteriormente não funciona mais**.

Regressões são um subconjunto de bugs. Usa-se o rótulo "regressão" para sugerir que é um defeito causando um retorno da funcionalidade.

O rótulo nos diz que algo funcionou antes e precisa de atenção extra de líder e produto para definir um prazo de resolução.

Porém o rótulo de "regressão" não se aplica em incidentes de novos recursos para os quais a funcionalidade nunca foi verificada como funcionando. Estes, por definição, não são regressões.

Uma regressão deve sempre ter o rótulo de "regressão: xx.x" para designar quando foi introduzido.

As regressões devem ser consideradas questões prioritárias que devem ser resolvidas o mais rápido possível, especialmente se tiverem um impacto severo nos usuários.

### Gestão de Incidentes ###
----
Priorização: Damos maior prioridade a regressões em recursos que funcionaram no último release mensal recente e nos atuais release canditate.
Os dois cenários abaixo podem ignorar a solicitação de exceção no processo de liberação, em que a versão de regressão afetada corresponde à versão de lançamento mensal atual.

- Uma regressão que funcionou no último release mensal
  * Exemplo: Em 11.0, lançamos um novo recurso X que é verificado como funcionando. Então na versão 11.1 o recurso não funciona mais, isso é regressão para 11.1. O problema deve ter no rótulo "regressão: 11.1".
  * Nota: Quando dizemos o último release mensal, isso pode se referir à versão atualmente em execução na produção ou à versão mais recente disponível no repositório de homologação.

- Uma regressão que funcionou nos release candidates atuaais
  * Exemplo: No 11.1-RC3, enviamos um novo recurso que foi verificado como funcionando. Então, em 11.1-RC5, o recurso não funciona mais, isso é regressão para 11.1. O problema deve ter no rótulo "regressão: 11.1".
  * Nota: Uma regressão pode ser relatada em um release antes de sua data de release 'oficial' no dia XX do mês.

Quando um incidente é encontrado:

1. Crie um problema descrevendo o problema da maneira mais detalhada possível.
2. Se possível, forneça links para exemplos reais e como reproduzir o problema.
3. Rotule o problema corretamente, usando o rótulo da equipe, o rótulo do assunto e qualquer outro rótulo que possa ser aplicado no caso específico
4. Notifique o respectivo Líder para avaliar e aplicar o rótulo de Gravidade e o rótulo de Prioridade.
5. O gestor do produto pode incluído para pesar na priorização, caso necessário.

6. Se o incidente não é uma regressão:
  - O Líder decide qual momento o incidente será corrigido.

  1. Se o incidente é uma regressão:
    - Determine o release que a regressão afeta e adicione o rótulo de regressão correspondente: xx.x.

  2. Se a versão de lançamento afetada não puder ser determinada, adicione o rótulo de regressão genérico.

  3. Se a versão afetada xx.x na "regressão: xx.x" for a release atual, recomenda-se realizar a correção na sprint atual.
     - Isso cai sob regressões que funcionaram na última versão e nos RCs atuais. Explicações mais detalhadas na seção Priorização acima.

  4. Se a versão afetada xx.x na regressão: xx.x é mais antigo que a versão atual
     - Se a regressão for uma gravidade S1, recomenda-se programar a correção para o marco atual. Gostaríamos de corrigir a maior regressão de severidade assim que pudermos.
     - Se a regressão for uma gravidade S2, S3 ou S4, a regressão pode ser programada para marcos posteriores, a critério do Gerente de Engenharia e do Gerente de Produto.