﻿Esta cerimônia é realizada após o Aceite da Proposta da Release e também após Detalhar as Histórias. 

Nesse momento os detalhes de negócio e possíveis discussões técnicas sobre as histórias que estão no Product Backlog devem fornecer informações suficientes para que os squads possam realizar o planejamento da próxima entrega/release.

Na reunião são apresentadas as histórias selecionadas, para os envolvidos revisar o conteúdo e detalhar (quebrar em subtarefas) tecnicamente os itens. Caso existam itens de ajustes de homologação da sprint anterior, esses também são analisados sobre a viabilidade de inclusão.

Participam dessa reunião, o Product Owner (analista de negócio no LabSoft), desenvolvedores e o [Gestor do Processo](/process/gestor-processo.md).

Roteiro da reunião

- Antes do início
     - Uma lista de itens especificados e aprovados pelo cliente (Aceite da Proposta da Release)					
	 - Embora a cerimônia tenha um momento específico de ocorrer é fundamental que o [Gestor do Processo](/process/gestor-processo.md) certifique-se da agenda de todos os envolvidos. Caso ocorra a necessidade de participação do cliente, definir previamente essa data com ele.
     - Definir a quantidade de tempo disponível pelo time (avaliar possíveis ausências, férias, etc)				
	- Todos os épicos e histórias aceitos, que estão no backlog do produto, devem estar no Próximo planejamento

- Durante
    - Cada item selecionado no Próximo Planejamento, os participantes revisam a especificação disponível. Um épico deve ser dividido em histórias. Se necessário, realizar a reescrita de um item para melhorar a compreensão e o desenvolvimento. Em caso de mudanças que impactom em requisitos de negócio, obter o novo aceite do cliente.
    - Em cada história, os squads envolvidos deverão quebras as tarefas de trabalho necessário. Existe um [padrão de tarefas](/process/develop/guia-padrao-tarefas.md) que deve ser utilizado, salvo possíveis excessões.					

- Após
    - ???
