|Descrição	                        | Web/Mobile/Ambos|
|-----------------------------------|------------
Layout	                            |       W
Script BD	                        |       W
Importação Base	                    |       W
Sincronia Exportação [nome entidade]|       A
Persistência [nome entidade]	    |       M
Sincronia Importação [nome entidade]|       A
UI - Tela [nome da tela]	        |       A
UI - Componente [nome do componente]|       A
UI - Prototipação                   |       A
Testes	                            |       A
Relatório Diário [nome layout]	    |       W
Extração Relatório	                |       W
Manual do Cliente	                |       A
Sincronia Consulta [nome]	        |       A
API Consulta [nome]                 |       A
API Envio [nome]                    |       A