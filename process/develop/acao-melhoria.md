Título da issue: Setup pilot test strategy retrospective for a team and collect feedback to improve


# Problema
If we move to focusing on internal tasks to improve our efficiency and that of development teams, we miss out on training and guidance opportunities we could offer to our counterpart teams. This could also create some distance between ourselves and our counterpart teams which can make future collaboration slightly more difficult to spin up again.

# Passos para mitigação
As a step to mitigate these issues, we can take on some quality coaching tasks such as getting everyone on a baseline through conducting an initial test strategy retrospective.

The test strategy retrospective is an opportunity for developers within a team to openly discuss their understanding of testing approaches and strategies in relation to their work. It helps everyone on the team:
*  understand what the team&#39;s test strategy is
*  agree on which parts of that strategy are implemented

This solidifies the language used (everyone understands what the team means when they say integration vs UI vs E2E vs system testing), eliminating confusion, and identifies test gaps for the team. It is not a code review performed by the quality counterpart, but takes input from the entire team on what they know about their testing practices. It gets people who are not testers to think about the types of testing that are happening and why. Typically, an artifact from such an endeavor would be some type of visual test coverage model that can be used as a baseline strategy for the team to refer to.

# Benefícios
As we step back to focus on internal issues, we leave counterpart teams with a documented test strategy that they have mostly defined themselves. It also leaves a positive impression from the quality team while giving us breathing room to focus on other necessary tasks.

# Challenges
*  My experience with these has been in person, not async. I&#39;m thinking a combination of a zoom meeting plus a retrospective issue may be sufficient. 
*  We will also have to compile the information into a visual test coverage model. As opposed to putting the onus on the facilitator, possible usage of Mermaid charting within our issues could allow everyone to collaborate and adjust the model as necessary. We could kickstart this by developing a basic mermaid chart in a template so contributors only need to make edits and don&#39;t have to figure out all of the Mermaid syntax and layout.

# Ongoing
For the counterpart team:
*  Take the knowledge gained in the test strategy retrospective and make sure that discussion on it continues in the regular team retrospectives where everyone will be encouraged to talk about testing that went well, didn&#39;t go well, can be improved, etc. Perhaps some updates to those templates would be necessary.

For the quality counterpart:
*  Review, start and encourage discussions on team retrospectives. Offer guidance in the retrospective on how the strategy could be improved.

# Próximos passos
* [ ]  Solidify format to facilitate async participation - create an example issue
* [ ]  Setup pilot test strategy retrospective for a team and collect feedback to improve