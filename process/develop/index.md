## Como estamos estruturados
Nos inspiramos no modelo organizacional do Spotify (Não conhece? Dá só uma olhada: Spotify Engineering Culture) para criar nossa estrutura, porém com adaptações para nossa realidade :-). Nosso objetivo é orientar o uso das ferramentas e processos de trabalho do LabSoft, buscando a padronização do preenchimento das tarefas para podermos verificar as atividades em execução e aprimorar os processos, promovendo melhoria contínua.
Assim sendo, estamos divididos da seguinte forma:

#### Produto
Um produto é composto de um ou mais sistemas que criam uma cadeia de entrega clara de valor para o negócio. Todo produto tem uma missão e visão definidos bem como seus principais indicadores de controle (métricas de saúde sistêmica) e de sucesso (métricas de sucesso do negócio).

#### Squads (ou Times)
Normalmente um por produto, é um agrupamento de engenheiros de perfis diversos, product manager, product analysts e product designers com conhecimento, autonomia e propósito claro (refletido pela missão, valor e métricas de sucesso do produto). Também podem fazer parte do time membros de chapters atuando de forma provisória ou definitiva.
Um time tem total responsabilidade sobre seu produto das seguintes formas:
- Definindo as features que terão maior impacto nas métricas de sucesso do produto.
- Implementando-as usando a melhor solução técnica possível, sempre levando em consideração sua qualidade, viabilidade e simplicidade.
- Definindo processos para que o time consiga extrair o máximo de sua performance.
- Suportando os sistemas que compõem o produto durante todo seu ciclo de vida.
- Garantindo a transparência de seu trabalho a todas as partes interessadas, incluindo aqui outros times que dependam da cadeia de valor entregue por esse produto.

#### Tribes
É um agrupamento de vários times, onde seus produtos tenham correlação de negócio e que possam ter dependência entre si. Existem para guiar os produtos em direção de um objetivo comum.

Toda tribe tem um tribe leader responsável por promover o fluxo de comunicação entre os times.

#### Chapters
Transversais às squads e tribes, os chapters são agrupamentos de profissionais de perfis semelhantes sob uma gestão funcional. Os membros normalmente atuam em times multifuncionais, por isso o chapter não é considerado um time por si só. Entre os motivos para a existência de chapters estão:

Compartilhamento de conhecimento específico e fluxo de informações entre profissionais desse perfil.
Definição do formato de trabalho e padrões.
Liderança por profissionais que tenham perfis similares e, portanto, melhores condições para apoiar os membros do chapter avançarem em suas carreiras.


#### Guildas
Também transversais às squads e tribes, as guildas são agrupamentos de profissionais com um objetivo comum normalmente lateral aos objetivos de produtos. As guildas:

São abertas para participação de quaisquer interessados.
Podem ter líderes mas não necessariamente gestão funcional.
Podem ser provisórias e deixarem de existir assim que o objetivo é cumprido ou
Podem ser contínuas quando o objetivo for mais conceitual, como “difundir o conhecimento de frontend na empresa”
Tem suas próprias cerimônias para promover alinhamento entre os membros da guilda.
Tem autonomia para tomada de decisões e definição de padrões quando em sintonia com seu objetivo.

### **Processo de Desenvolvimento** ###
----

- **Squad de Desenvolvimento (Web e Mobile)**
  * [Product Backlog](/process/develop/products-backlog.md)
  * [Planejamento próxima release/entrega](/process/develop/planejar-entrega-release.md)
  * [Reunião diária](/process/develop/reuniao-diaria.md)
  * Análise de Replanejamento
  * Gestão dos Incidentes (bugs)
  * Retrospectiva das Releases e kickoff novos produtos/projetos
    * [Retrospectivas](/process/develop/Retrospectivas.md)
    * [Kickoff novos produtos/projetos](/process/develop/Kickoff.md)
  * Guias
    * [Padrões de tarefas](/process/develop/guia-padrao-tarefas.md)
    * [Template para Elaborar Incidentes no Jira](/process/develop/guia-modelo-incidente.md)
    * [Boas práticas no uso do Jira](/process/develop/guia-praticas-jira.md)
    * [Template para Elaborar Análise de Negócio no Jira](/process/develop/guia-modelo-analise-negocio.md)
    * [Padrões para especificação em Análise de Negócio](/process/develop/guia-padrao-analise-negocio.md)
    * [Padrões para estilos CSS](/process/develop/guia-padrao-css.md)
    * [Padrões para HTML](/process/develop/guia-padrao-html.md)
    * [Checklist para testes - Portal](guia-testes-checkitens-portal.md)
- **Squad de Produto (Portifólio e Negócio)**
  * [Backlog de Demandas do Produto](/process/develop/products-backlog.md)
  * Análise das Demanda priorizadas
  * [Portfólio Backlog](/process/develop/products-backlog.md)
  * [Proposta para Release](/process/product/proposta-release.md)
  * Aceite da Proposta de Release
  * [Produtos e Roadmap](/process/product/products-roadmap.md)
  * Estratégia dos Produtos
  * [Revisão estratégica e operacional](/process/product/revisao-estrategica-operacional.md)

  
### **Ambientes de desenvolvimento e Arquitetura dos produtos** ###
----

- Mobile/Java


- Web/Java

   - Ambiente padrão


   - [Servidor WildFly de Testes](/process/develop/install-wildfly.md)


- Angular




<script src="https://gitlab.com/trellis/snippets/1753038.js"></script>
