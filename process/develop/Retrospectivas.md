Após cada lançamento da release em produção, realizamos uma reunião retrospectiva. Nela discutimos o que deu certo, o que deu errado e o que podemos melhorar para o próximo release/entrega.

Em cada retrospectiva, escolhemos um número baixo (1-3) de melhorias de alto impacto que podem ser realisticamente entregues antes do próximo lançamento da release. Uma pessoa do squad envolvido é definida para entregar cada melhoria. Antes do início da próxima retrospectiva, espera-se que as pessoas resolvam seus itens de ação e atualizem as suas tarefas de melhoria. E falamos sobre o status de cada melhoria no início da retrospectiva a seguir.

As notas estão disponíveis em uma tarefa acessível publicamente. Consulte a tarefa para obter detalhes.

Como criar uma melhoria
1. Abra a ferramenta de tarefas do Tetris
2. Antes de criar uma nova tarefa, verifique no backlog de melhorias se já não existe um registro do assunto relacionado
   * Se houver, complemente a tarefa com seu comentário
3. Se realmente for uma nova melhoria crie uma tarefa
   * Coloque as seguintes etiquetas: