### Lista de práticas adotadas para a especificação de requisitos de negócio no Jira. ###
----

* Mensagens com decisão:  **[MSG:"texto da mensagem" OP:"nome dos botões separados por /"]** - Exemplo: [MSG: "O EC deve apresentar o mesmo MCC do EC modelo" OP: "Confirmar/Fechar"].
* Mensagens de alerta: **[MSG:"texto da mensagem" OP:"Padrão Alert"]** - Exemplo: [MSG: "Representa o Volume total faturado pelo cliente em todos os meios de pagamento (dinheiro, cartão, cheque, etc)" OP:"Padrão Alert"]
* Referenciar a localização de uma funcionalidade em subníveis do aplicativo: **usar o símbolo >** - Exemplo: Clientes>Aba Adquirência>Atualização
* Especificar campos layout  **[CAMPO: "Nome do campo"]** - Exemplo [CAMPO: "Volume total faturado"]
