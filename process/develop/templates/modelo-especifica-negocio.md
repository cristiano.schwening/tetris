﻿O conteúdo do modelo abaixo deve ser usado no campo "Descrição" nas pendências da ferramenta Jira. Esse mesmo modelo está disponível em http://jira.xxx.com.br/browse/CVD-4706 para os clientes, dessa forma uma atualização no modelo aqui deve refletir nesse outro local.

O Jira usa uma formatação de marcação diferente do Markdown, usado aqui no GitLab, dessa forma o texto aparece com as marcações específicas. 

Quando o texto for colocado no campo descrição do Jira, essas marcações serão formatadas e o texto será visualizado corretamente.

Mais informações sobre a marcação do editor do Jira podem ser obtidas aqui: http://jira.xxx.com.br/secure/WikiRendererHelpAction.jspa?section=texteffects
__________
```
h3. *1. Problema para resolver* 

*Para o executivo de contas deseja-se atribuir aos eventos 3594 e 3595 uma melhoria na seleção de motivos e classificações adequadas para tabulação e tratamento da área responsável, o SMART GV/Portal deverá melhorar a usabilidade para o executivo com seleção de motivos apropriados e apresentar relatórios apartados para melhor acompanhamento dos casos pendentes, conforme necessidades a seguir.*

h3. *2. Proposta de solução*

RN1: No módulo GAC>Retorno de GAC, na opção “Evento a ser tratado” quando o evento selecionado for 3594 (abertura de GAC) e 3595 (reincidência de GAC).
  - RN1.1: O campo “Retorno do Tratamento” não deverá ser apresentado para o usuário.
  - RN1.2: O campo “Motivo do Retorno” deverá conter a lista de opções para seleção do usuário. Os itens atualmente cadastrados deverão ser removidos e substituído pelos novos descritos abaixo:
{quote}
Decisor Não Encontrado
Retido com Postecipado e/ou MDR
Retido com outras ofertas (Controle, Mobile, Livre, etc)
Retido sem oferta
{quote}

RN2: O campo “Baixa do Atendimento” deverá permanecer sem alteração.
 
h3. *3. O que é esperado como resultado?*
 
O executivo comercial  poderá realizar a tratativa de GAC seguindo uma ordem mais adequada para o preenchimento dos motivos por seleção, sem obrigatoriedade de digitação. 
Melhorar controle da área responsável por tratar o retorno da GAC através da tabulação padronizada e relatório segregado os casos ainda pendentes.

h3. *4. Protótipos*

h3. *5. Links e Referências*
```
____________
