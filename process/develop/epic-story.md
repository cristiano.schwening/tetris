Utilizamos **Épicos de Negócio** para registrar uma grande história de usuário que pode representar uma funcionalidade ou uma iniciativa maior de negócio que será quebrada em partes menores, mais mensuráveis e as vezes abrangendo várias sprints. Bem como, grandes iniciativas do cliente organizadas em portfólios e backlogs e priorizadas no nível de negócio considerando o potencial ROI para implementação. Um épico tem seu tempo fixo, todo épico tem a data em que pretende ser completado. 

Por outro lado, usamos as **História do usuário** para representar a descrição de uma necessidade do usuário para o produto (ou seja, de um “requisito”) sob o ponto de vista dele. Busca descrever essa necessidade de uma forma simples e compreensível. Em virtude da complexidade das demandas a maior parte das nossas histórias possuem maiores detalhes de negócios que são documentados na própria história seguindo um dos padrões a seguir. 

Por padrão na VEK é utilizado dois tipos de modelos de escrita dos detalhes da história: 
* Mudanças em funcionalidades existentes
* Novas funcionalidades. 

Para o registro de defeitos, utilizamos outro padrão que está descrito no item Gestão de Incidentes.


Quer saber mais:

* Para você se aprofundar nos conceitos de história de usuários, recomendamos esse [**link**](https://www.romanpichler.com/wp-content/uploads/2013/06/WritingGreatUserStories.pdf) e também o canal #agile no nosso slack.

